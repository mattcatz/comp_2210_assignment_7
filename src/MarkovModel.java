import java.io.*;
import java.util.*;

/**
 * MarkovModel.java
 * Creates an order K Markov model of the supplied source text.
 * The value of K determines the size of the "kgrams" used
 * to generate the model. A kgram is a sequence of k consecutive
 * characters in the source text.
 *
 * @author     Matthew Cather (mrc0033@auburn.edu)
 * @author     Dean Hendrix (dh@auburn.edu)
 * @version    2015-11-16
 *
 */
public class MarkovModel {
   private ArrayList<String> kgramArray =  new ArrayList<>();
   private HashMap<String, String> kgrams = new HashMap<>();
   private BufferedReader currentKgram;

   /**
    * Construct the order K model of the file sourceText.
    */
   public MarkovModel(int K, File sourceText) throws IOException {
      currentKgram = new BufferedReader(new FileReader(sourceText));
      LinkedList<String> buffer = new LinkedList<>();
      int i = currentKgram.read();
      while (i > -1) {
         buffer.add(String.valueOf(i));
         i = currentKgram.read();
      }
   }


   /**
    * Construct the order K model of the string sourceText.
    */
   public MarkovModel(int K, String sourceText) {
      int i = 0;
      String kgram;
      String next;
      while (i <= sourceText.length()) {
         kgram = sourceText.substring(i, i + K);
         i += K;
         if (i < sourceText.length()) {
            next = String.valueOf(sourceText.charAt(i + 1));
         } else {
            next = String.valueOf('\u0000');
         }
         if (!kgrams.containsKey(kgram)) {
            kgramArray.add(kgram);
            kgrams.put(kgram, next);
         } else {
            kgrams.put(kgram, kgrams.get(kgram) + next);
         }
      }
   }

   /** Return the first kgram found in the source text. */
   public String firstKgram() {
      return kgramArray.get(0);
   }


   /** Return a random kgram from the source text. */
   public String randomKgram() {
      Random i = new Random();
      return kgramArray.get(i.nextInt());
   }


   /**
    * Return a single character that follows the given
    * kgram in the source text. Select this character
    * according to the probability distribution of all
    * characters the follow the given kgram in the
    * source text.
    */
   public char nextChar(String kgram) {
      Random temp = new Random();
      String temp2 = kgrams.get(kgram);
      return temp2.charAt(temp.nextInt(kgram.length()));
   }

}
