import java.io.File;
import java.io.IOException;

/**
 * TextGenerator.java.
 * Creates an order K Markov model of the supplied
 * source text, and then outputs M characters generated
 * according to the model.
 *
 * @author     YOUR NAME (you@auburn.edu)
 * @author     Dean Hendrix (dh@auburn.edu)
 * @version    2015-11-16
 *
 */
public class TextGenerator {

   /** Drives execution. */
   public static void main(String[] args) {

      // No error checking!
      int K = Integer.parseInt(args[0]);
      int M = Integer.parseInt(args[1]);
      File text = new File(args[2]);

      //MarkovModel lm = new MarkovModel(K, text);

      // text generation logic goes here
   }
}
